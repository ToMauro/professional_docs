import { mkdir, readdir, readFile } from 'node:fs/promises'
import { resolve } from 'node:path'
import nunjucks from 'nunjucks'
import puppeteer from 'puppeteer'

const dataDir = './data'
const distDir = './dist'

async function html2Pdf (name, html) {
  const browser = await puppeteer.launch({
    // Uncomment for debugging
    // headless: false,
    // slowMo: 250, // slow down by 250ms
    // devtools: true
  })
  const page = await browser.newPage()
  await page.setContent(html)
  await page.pdf({
    path: `${distDir}/${name}.pdf`,
    format: 'A4',
    margin: {
      top: '20mm',
      right: '20mm',
      bottom: '20mm',
      left: '20mm'
    }
  })
  // Uncomment for debugging
  // await page.evaluate(() => {
  //   debugger;
  // });
  await browser.close()
}

async function buildPdf () {
  try {
    const files = await readdir(dataDir)
    files.forEach(async (fileName) => {
      const name = fileName.split('.')[0]
      const filePath = resolve(`./data/${fileName}`)
      const data = await readFile(filePath, { encoding: 'utf8' })
      const html = nunjucks.render('resume.html', JSON.parse(data))
      html2Pdf(name, html)
    })
  } catch (e) {
    console.log(`Error in buildPdf: ${e}`)
  }
}

async function main () {
  try {
    await mkdir(distDir, { recursive: true })
    buildPdf()
  } catch (e) {
    console.log(`Error in main: ${e}`)
  }
}

main()
